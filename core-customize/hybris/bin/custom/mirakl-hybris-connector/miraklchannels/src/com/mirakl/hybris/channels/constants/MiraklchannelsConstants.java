package com.mirakl.hybris.channels.constants;

@SuppressWarnings({"deprecation","squid:CallToDeprecatedMethod"})
public class MiraklchannelsConstants extends GeneratedMiraklchannelsConstants
{
	public static final String EXTENSIONNAME = "miraklchannels";

	private MiraklchannelsConstants()
	{
		//empty
	}

	public static final String MIRAKL_CHANNEL_SESSION_ATTRIBUTE = "mirakl_channel";
	public static final String MIRAKL_CHANNELS_PROPERTY_PREFIX = "mirakl.channels.";
	public static final String MIRAKL_CHANNELS_PROPERTY_SEPARATOR = ".";
	public static final String MIRAKL_CHANNELS_ENABLED = "mirakl.channels.enabled";
	public static final String SOLR_MIRAKL_CHANNEL_PARAMETER = "mirakl-channel";
	public static final String SOLR_PROPERTY_TO_REPLACE_PARAMETER = "property-to-replace";

}
