package com.mirakl.hybris.facades.shop;

import com.mirakl.hybris.beans.EvaluationPageData;
import com.mirakl.hybris.beans.ShopData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.servicelayer.data.PaginationData;

public interface ShopFacade {
  /**
   * Returns the Shop data matching the given id
   *
   * @param id the id of the shop
   * @return Shop data
   */
  ShopData getShopForId(String id);

  /**
   * Returns the evaluations for the designated shop
   *
   * @param id the id of the shop
   * @param pageableData filled with the page size and the current page
   * @return the requested page of evaluations
   * @deprecated use use {@link com.mirakl.hybris.facades.shop.ShopFacade#getShopEvaluationPage(String, PaginationData)} instead
   */
  @Deprecated(since = "2.8.0", forRemoval = true)
  EvaluationPageData getShopEvaluationPage(String id, PageableData pageableData);

  /**
   * Returns the evaluations for the designated shop
   *
   * @param id the id of the shop
   * @param paginationData filled with the page size and the current page
   * @return the requested page of evaluations
   */
  EvaluationPageData getShopEvaluationPage(String id, PaginationData paginationData);
}
