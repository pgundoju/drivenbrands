package com.mirakl.hybris.facades.setting;

import java.util.List;
import java.util.Map;

import com.mirakl.client.mmp.domain.reason.MiraklReasonType;
import com.mirakl.hybris.beans.ReasonData;

public interface ReasonFacade {

  /**
   * Get the reasons with the given type as defined in Mirakl
   *
   * @param type the type of reasons to retrieve
   * @return a list of Reasons
   */
  List<ReasonData> getReasons(MiraklReasonType type);


  /**
   * Get the reasons with the given type as defined in Mirakl and returns a map of code/label
   * 
   * @param type the type of reasons to retrieve
   * @return a map having for key the reason code and for value the reason label
   */
  Map<String, String> getReasonsAsMap(MiraklReasonType type);

}
