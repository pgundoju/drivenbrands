package com.mirakl.hybris.core.setting.services.impl;

import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.reason.MiraklGenericReason;
import com.mirakl.client.mmp.domain.reason.MiraklReason;
import com.mirakl.client.mmp.domain.reason.MiraklReasonType;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApi;
import com.mirakl.client.mmp.request.reason.MiraklGetReasonsByTypeRequest;
import com.mirakl.client.mmp.request.reason.MiraklGetReasonsRequest;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultReasonServiceTest {

  @InjectMocks
  private DefaultReasonService reasonService;

  @Captor
  private ArgumentCaptor<MiraklGetReasonsByTypeRequest> requestArgumentCaptor;
  @Mock
  private MiraklMarketplacePlatformFrontApi miraklApi;
  @Mock
  private List<MiraklGenericReason> miraklGenericReasons;
  private List<MiraklReason> miraklReasons;

  @Before
  public void setUp() throws Exception {
    miraklReasons = asList( //
        reason("code-1", "label-1", MiraklReasonType.OFFER_MESSAGING), //
        reason("code-2", "label-2", MiraklReasonType.INCIDENT_OPEN)
        );
  }

  @Test
  public void shouldGetReasons() throws Exception {
    when(miraklApi.getReasons(any(MiraklGetReasonsRequest.class))).thenReturn(miraklReasons);

    List<MiraklReason> reasons = reasonService.getReasons();

    assertThat(reasons).hasSize(miraklReasons.size());
  }

  @Test
  public void shouldGetReasonsByType() throws Exception {
    when(miraklApi.getReasonsByType(requestArgumentCaptor.capture())).thenReturn(miraklGenericReasons);

    List<MiraklGenericReason> orderMessagingReasons = reasonService.getReasonsByType(MiraklReasonType.ORDER_MESSAGING);

    MiraklGetReasonsByTypeRequest request = requestArgumentCaptor.getValue();
    assertThat(request.getReasonType()).isEqualTo(MiraklReasonType.ORDER_MESSAGING);
    assertThat(orderMessagingReasons).isEqualTo(miraklGenericReasons);
  }

  private MiraklReason reason(String code, String label, MiraklReasonType type) {
    MiraklReason reason = new MiraklReason();
    reason.setCode(code);
    reason.setLabel(label);
    reason.setType(type);
    return reason;
  }

}
