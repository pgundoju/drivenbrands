package com.mirakl.hybris.core.pagination.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.servicelayer.data.PaginationData;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

@Deprecated(since = "2.8.0", forRemoval = true)
public class MiraklPaginationDataPopulator implements Populator<PageableData, PaginationData> {

  @Override
  public void populate(PageableData pageableData, PaginationData paginationData) throws ConversionException {
    validateParameterNotNullStandardMessage("pageableData", pageableData);
    validateParameterNotNullStandardMessage("paginationData", paginationData);

    paginationData.setCurrentPage(pageableData.getCurrentPage());
    paginationData.setPageSize(pageableData.getPageSize());
  }

}
