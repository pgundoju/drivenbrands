package com.mirakl.hybris.core.setting.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mmp.domain.reason.MiraklGenericReason;
import com.mirakl.client.mmp.domain.reason.MiraklReason;
import com.mirakl.client.mmp.domain.reason.MiraklReasonType;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApi;
import com.mirakl.client.mmp.request.reason.MiraklGetReasonsByTypeRequest;
import com.mirakl.client.mmp.request.reason.MiraklGetReasonsRequest;
import com.mirakl.hybris.core.setting.services.ReasonService;

public class DefaultReasonService implements ReasonService {

  protected MiraklMarketplacePlatformFrontApi miraklApi;

  @Override
  public List<MiraklReason> getReasons() {
    return miraklApi.getReasons(new MiraklGetReasonsRequest());
  }

  @Override
  public List<MiraklGenericReason> getReasonsByType(final MiraklReasonType miraklReasonType) {
    return miraklApi.getReasonsByType(new MiraklGetReasonsByTypeRequest(miraklReasonType));
  }

  @Required
  public void setMiraklApi(MiraklMarketplacePlatformFrontApi miraklApi) {
    this.miraklApi = miraklApi;
  }

}
