package com.mirakl.hybris.core.setting.services;

import java.util.List;

import com.mirakl.client.mmp.domain.reason.MiraklGenericReason;
import com.mirakl.client.mmp.domain.reason.MiraklReason;
import com.mirakl.client.mmp.domain.reason.MiraklReasonType;

public interface ReasonService {

  /**
   * Gets reasons from Mirakl (RE01)
   *
   * @return a list of MiraklReason from Mirakl
   */
  List<MiraklReason> getReasons();

  /**
   * Gets reasons from Mirakl filtered by type (RE02)
   *
   * @param miraklReasonType the requested type to retrieve
   * @return a list of MiraklReason from Mirakl
   */
  List<MiraklGenericReason> getReasonsByType(MiraklReasonType miraklReasonType);

}
