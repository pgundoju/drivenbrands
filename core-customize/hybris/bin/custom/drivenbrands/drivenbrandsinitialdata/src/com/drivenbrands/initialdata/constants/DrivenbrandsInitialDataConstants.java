/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.drivenbrands.initialdata.constants;

/**
 * Global class for all DrivenbrandsInitialData constants.
 */
public final class DrivenbrandsInitialDataConstants extends GeneratedDrivenbrandsInitialDataConstants
{
	public static final String EXTENSIONNAME = "drivenbrandsinitialdata";

	private DrivenbrandsInitialDataConstants()
	{
		//empty
	}
}
