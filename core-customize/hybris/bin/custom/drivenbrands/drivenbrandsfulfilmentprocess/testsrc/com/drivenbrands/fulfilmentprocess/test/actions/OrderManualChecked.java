/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.drivenbrands.fulfilmentprocess.test.actions;

/**
 * Test counterpart for {@link com.drivenbrands.fulfilmentprocess.actions.order.OrderManualCheckedAction}
 */
public class OrderManualChecked extends TestActionTemp
{
	//EMPTY
}
