/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.drivenbrands.test.constants;

/**
 * 
 */
public class DrivenbrandsTestConstants extends GeneratedDrivenbrandsTestConstants
{

	public static final String EXTENSIONNAME = "drivenbrandstest";

}
